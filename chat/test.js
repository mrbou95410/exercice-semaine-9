let express = require('express');
let app = express();
let http = require('http').createServer (app);
let io = require('socket.io')(http);

app.use(express.static('public'));

app.get('/', function(require, response){
    response.sendfile('index.html');
});

io.on('connection', function(socket){
    console.log('Vous êtes connecté');
    socket.on('disconnect', function(){
        console.log('Vous êtes déconnecté');
    });
    socket.on('chat message', function(msg){
        console.log('message: ' + msg);
        io.emit('chat message', msg);
    });
});

http.listen(3000, function(){
    console.log('listen to *:3000');
});