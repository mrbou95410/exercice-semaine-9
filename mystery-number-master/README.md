# Le nombre mystère

Le nombre mystère avec un twist!

## Objectif
- Utiliser un formulaire
- Découvrir l'éco-système des packages NPM
- Reprendre un exo déjà réalisé

## Contexte
NodeJS est une boîte à outils où on trouve tout ce dont on a besoin.
Comme pour jQuery, reste à se poser la question de l'éco-conception et de la stack technique. 

## Compétence.s concernée.s
- Réaliser une interface utlisateur web
- Développer un interface utilisateur dynamique

## Critères de réussite
- Le code est **indenté** et **lisible**.
- Il n’y a pas d’erreurs dans le code.

## Livrable.s
> Dimanche 23h59
- **Lien vers le repo gitlab**.
- **Lien vers la version en ligne**.

## Réalisation attendues
- Faire deviner un nombre aléatoire entre 1 et 10
- 3 essais
- En cas de mauvaise réponse, indiquer si la réponse fournie par l'utilisateur est trop grande ou trop petite
- Ne pas utiliser de Math.random
- Utiliser Express