const express = require("express");
const app = express();
const port = 1337;
const bodyParser = require('body-parser');
let random = require("random-number-generator");
let nombre = random(10, 0);
console.log(nombre);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('./'));

app.get('/', (req, res) => res.sendFile("index.html"));
app.post('/nombrealea', (request, response) => {
    console.log(request.body.number);
        if(request.body.number == nombre){
            response.send("Tu as gagné le droit de me donner 10 euros");
        }
        else if(request.body.number >= nombre) {
            response.send("Inférieur <a href='http://localhost:1337'>Retente ta chance</a>");
        }
        else if(request.body.number <= nombre) {
            response.send("Supérieur <a href='http:localhost:1337'>Allez réessaye encore</a>");
        }
});


app.listen(port, () => console.log(`Example app listening on port ${port}!`));
